<h1>Transfer Learning</h1>

In transfer learning, the knowledge of an already trained machine learning model is applied to a different but related problem. For example, if we trained a simple classifier to predict whether an image contains a backpack, we could use the knowledge that the model gained during its training to recognize other objects like sunglasses.

With transfer learning, we basically try to exploit what has been learned in one task to improve generalization in another. We transfer the weights that a network has learned at "task A" to a new "task B."
In computer vision, for example, neural networks usually try to detect edges in the earlier layers, shapes in the middle layer and some task-specific features in the later layers. In transfer learning, the early and middle layers are used and we only retrain the latter layers. It helps leverage the labeled data of the task it was initially trained on.

![alt text](images/1.png?raw=true "Optional Title")

Transfer learning has several benefits, but the main advantages are  saving training time, better performance of neural networks (in most cases), and not needing a lot of data.

<h1>CNN as a Feature Extracter</h1>

Several pre-trained models used in transfer learning are based on large convolutional neural networks (CNN). In general, CNN was shown to excel in a wide range of computer vision tasks. Its high performance and its easiness in training are two of the main factors driving the popularity of CNN over the last years.

A typical CNN has two parts:

**Convolutional base** which is composed by a stack of convolutional and pooling layers. The main goal of the convolutional base is to generate features from the image.

**Classifier** which is usually composed by fully connected layers. The main goal of the classifier is to classify the image based on the detected features. A fully connected layer is a layer whose neurons have full connections to all activation in the previous layer.

![alt text](images/fine_tune.png?raw=true "Optional Title")

One important aspect of these deep learning models is that they can automatically learn hierarchical feature representations. This means that features computed by the first layer are general and can be reused in different problem domains, while features computed by the last layer are specific and depend on the chosen dataset and task. As a result, the convolutional base of our CNN — especially its lower layers (those who are closer to the inputs) — refer to general features, whereas the classifier part, and some of the higher layers of the convolutional base, refer to specialised features.

When we use a pre-trained model for our new data, we have to fine tune our convolutional layers. Following steps will be taken to **fine tune the pre-trained model** according to our requirements.

1. **Train the entire model**. In this case, you use the architecture of the pre-trained model and train it according to your dataset. You’re learning the model from scratch, so you’ll need a large dataset (and a lot of computational power).

2. **Train some layers and leave the others frozen** As you remember, lower layers refer to general features (problem independent), while higher layers refer to specific features (problem dependent). Here, we play with that dichotomy by choosing how much we want to adjust the weights of the network (a frozen layer does not change during training). Usually, if you’ve a small dataset and a large number of parameters, you’ll leave more layers frozen to avoid overfitting. By contrast, if the dataset is large and the number of parameters is small, you can improve your model by training more layers to the new task since overfitting is not an issue.

3. **Freeze the convolutional base** This case corresponds to an extreme situation of the train/freeze trade-off. The main idea is to keep the convolutional base in its original form and then use its outputs to feed the classifier. You’re using the pre-trained model as a fixed feature extraction mechanism, which can be useful if you’re short on computational power, your dataset is small, and/or pre-trained model solves a problem very similar to the one you want to solve.

<h1>Create Deep learning model from scratch</h1>

We created a model for semantic road segmentation based on **U-Net Architecture** (U-Net is a convolutional neural network that was developed for biomedical image segmentation at the Computer Science Department of the University of Freiburg, Germany). The model was trained from over 700 road images.

Following were the results found:

**Accuracy**

![alt text](images/Scratch_Model_Accuracy.jpeg?raw=true "Optional Title")

**Detection on Test Image**

![alt text](images/Scratch_Model_Detection.jpeg?raw=true "Optional Title")

**Architecture**

![alt text](images/Scratch_Model_Arch.jpeg?raw=true "Optional Title")

<h1>Use Pre-Trained model for our data</h1>

We use **MobileNetV2 neural network architecture** as the encoder which is pre-trained on million of images.

Below are the results found:

**Accuracy**

![alt text](images/Transfer_Model_Accuracy.jpeg?raw=true "Optional Title")

**Detection on Test Image**

![alt text](images/Transfer_Model_Detection.jpeg?raw=true "Optional Title")

**Architecture**

![alt text](images/Transfer_Learning_Arch.jpeg?raw=true "Optional Title")

Here we observe that we have attained an **accuracy of more than 95%** in transfer learning model compared to our own model(90%).