import tensorflow as tf
from tensorflow_examples.models.pix2pix import pix2pix
from IPython.display import clear_output
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
import os
import cv2
import numpy as np

PathD = '/content/drive/My Drive/Image_Segmentation/Data/SingleClassRoadDataset'
images = []
labels = []

for image in os.listdir(PathD):
  if "_GT" in image:
    curr_label = cv2.imread(os.path.join(PathD, image),0)
    curr_label = np.asanyarray(curr_label, np.float32)/255.0
    labels.append(curr_label)
  if "_GT" not in image:
    curr_img = cv2.imread(os.path.join(PathD, image),1)
    images.append(curr_img)

images_all = images
indices = np.random.choice(range(len(images)), replace = False ,size = 400)
images = np.array(images)[indices]
labels = np.array(labels)[indices]
labels = np.expand_dims(labels,axis=-1)

(trainImg, testImg, trainLabel, testLabel) = train_test_split(images,
	labels, test_size=0.20, random_state=42)
trainImgs = np.array(trainImg)
trainLabels = np.array(trainLabel)
testImgs = np.array(testImg)
testLabels = np.array(testLabel)

OUTPUT_CHANNELS = 2  # Road and Non Road


##################################
# Get the MobileNet V2 as encoder#
##################################
base_model = tf.keras.applications.MobileNetV2(input_shape=[128, 128, 3], include_top=False)

# Use the activations of these layers
layer_names = [
    'block_1_expand_relu',   # 64x64
    'block_3_expand_relu',   # 32x32
    'block_6_expand_relu',   # 16x16
    'block_13_expand_relu',  # 8x8
    'block_16_project',      # 4x4
]
layers = [base_model.get_layer(name).output for name in layer_names]

# Create the feature extraction model
down_stack = tf.keras.Model(inputs=base_model.input, outputs=layers)

down_stack.trainable = False 


up_stack = [
    pix2pix.upsample(512, 3),  # 4x4 -> 8x8
    pix2pix.upsample(256, 3),  # 8x8 -> 16x16
    pix2pix.upsample(128, 3),  # 16x16 -> 32x32
    pix2pix.upsample(64, 3),   # 32x32 -> 64x64
]

def unet_model(output_channels):
  inputs = tf.keras.layers.Input(shape=[128, 128, 3])
  x = inputs

  # Downsampling through the model
  skips = down_stack(x)
  x = skips[-1]
  skips = reversed(skips[:-1])

  # Upsampling and establishing the skip connections
  for up, skip in zip(up_stack, skips):
    x = up(x)
    concat = tf.keras.layers.Concatenate()
    x = concat([x, skip])

  # This is the last layer of the model
  last = tf.keras.layers.Conv2DTranspose(
      output_channels, 3, strides=2,
      padding='same')  #64x64 -> 128x128

  x = last(x)

  return tf.keras.Model(inputs=inputs, outputs=x)

model = unet_model(OUTPUT_CHANNELS)
model.compile(optimizer="adam", loss="binary_crossentropy", metrics=['accuracy'])

TRAIN_LENGTH = trainImgs.shape[0]
BATCH_SIZE = 64
BUFFER_SIZE = 1000
STEPS_PER_EPOCH = TRAIN_LENGTH // BATCH_SIZE

EPOCHS = 20
VAL_SUBSPLITS = 5
VALIDATION_STEPS = testImgs.shape[0]//BATCH_SIZE//VAL_SUBSPLITS

model_history = model.fit(trainImgs, trainLabels, epochs=EPOCHS,
                          steps_per_epoch=STEPS_PER_EPOCH,
                          validation_steps=VALIDATION_STEPS,
                          validation_data=testImgs])
